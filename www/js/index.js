// マップオブジェクト
var MapObj = null;

// 表示範囲
var MapArea = [{
		"lat": 35.026461,
		"lng": 135.767275
	},
	{
		"lat": 34.979079,
		"lng": 135.717909
	}
];

// Marker座標リスト
var MarkerList = [];

// Markerオブジェクトリスト
var showingMarkers = [];

//Marker座標リスト生成
initMarkerList();

// 100個座標を生成する
function initMarkerList() {
	for (var i = 0; i < 100; i++) {
		var rand_lat = Math.random() * (MapArea[0]["lat"] - MapArea[1]["lat"]) + MapArea[0]["lat"];
		var rand_lng = Math.random() * (MapArea[0]["lng"] - MapArea[1]["lng"]) + MapArea[0]["lng"];
		MarkerList.push({
			"lat": rand_lat,
			"lng": rand_lng
		});
	}
}

// 初期化開始
function init() {
	// マップ初期化
	initMap().then(function() {
		// マーカー追加
		addMarkers().then(function() {
			// マーカー削除
			removeMarkers().then(function() {
				// マップクリーン
				output("msg3", "complete!");
				setTimeout(function() {
					MapObj.clear(
						function() {
							output("msg1", "");
							output("msg2", "");
							output("msg3", "");
							// 再開
                            setTimeout(init,1000);
						}
					);
				}, 2000);
			});
		});
	});
}

// マップ初期化
function initMap() {
	var d = jQuery.Deferred();
	var mapDiv = document.getElementById("MapDiv");

	if (null == MapObj) {
		var options = {
			'controls': {
				'myLocationButton': true
			},
			'camera': {
				'target': MarkerList,
				'zoom': 16
			}
		};
		MapObj = plugin.google.maps.Map.getMap(mapDiv, options)
		MapObj.one(plugin.google.maps.event.MAP_READY, function() {
			d.resolve();
		});
	} else {
		MapObj.setDiv(mapDiv);
		d.resolve();
	}

	return d.promise();
}

//マーカーを表示する
function addMarkers() {
	var d = jQuery.Deferred();

	var index = 0;
	var all_markers = MarkerList.length;

	var next = function(index) {
		var one = MarkerList[index];
		MapObj.addMarker({
			'position': one
		}, function(marker) {
			showingMarkers.push(marker);
			index++;
			output("msg1", "add marker:" + index);

			if (index == all_markers) {
				d.resolve();
			} else {
				next(index);
			}
		});
	};
	if (index == 0) {
		next(0);
	}

	return d.promise();
}

// マーカーを削除
function removeMarkers() {
	var d = jQuery.Deferred();

	var all_len = showingMarkers.length;

	var index = 0;

	var next = function(index) {
		if (index < all_len) {
			showingMarkers[index].remove(function() {
				index++;
				output("msg2", "remove marker:" + index);
				next(index);
			});
		} else {
			console.log("remove completed");
			showingMarkers = [];

			d.resolve();
		}
	};

	if (index == 0) {
		next(0);
	}

	return d.promise();
}

// ステータス表示
function output(id, str) {
	$("#" + id).html(str);
}

// deviceready
document.addEventListener('deviceready', init, false);
